"use strict";

(function() {

    let EpicApplication = {
        imageData: [],
        currentImageType: "natural",
        dateCache: {},
        imageCaches: {
            aerosol: new Map(),
            cloud: new Map(),
            enhanced: new Map(),
            natural: new Map()
        }
    };

    document.addEventListener("DOMContentLoaded", function(event) {
        
        recentAvailableDates();
        const imageMenu = document.getElementById("image-menu");

        /**
         * Creates a search URL based on the type of image and date.
         * @param {string} type - The type of the image.
         * @param {string} date - The date of the image.
         * @returns {string} The URL to fetch image data.
         */
        function createSearchURL(type, date) {
            let baseURL = 'https://epic.gsfc.nasa.gov/api';
            return `${baseURL}/${type}/date/${date}`;
        }

        /**
         * Updates the image menu with available image data.
         */
        function updateImageMenu(data) {
            const imageMenu = document.getElementById("image-menu");
            imageMenu.innerHTML = "";

            if (!data || data.length === 0) {
                let li = document.createElement("li");
                li.textContent = "No images available";
                imageMenu.appendChild(li);
            } else {
                data.forEach((image, index) => {
                    let li = document.createElement("li");
                    li.textContent = image.date;
                    li.setAttribute("data-index", index);
                    imageMenu.appendChild(li);
                });
            }
        }

        /**
         * Displays the selected image based on the user's click.
         * @param {Event} event - The event object.
         */
        function displayImage(event) {
            const listLi = Array.from(event.currentTarget.children);
            let liIndex = 0; 

            for(let i = 0; i < listLi.length; i++){
                if(listLi[i].textContent.includes(event.target.textContent)){
                    liIndex = i;
                    break; 
                }
            }
           
            const selectedData = EpicApplication.imageData[liIndex];
            const yearMonthDay = selectedData.date.substring(0, 10).split("-");
            const year = yearMonthDay[0]; 
            const month = yearMonthDay[1];
            const day = yearMonthDay[2];
            
            const type = document.getElementById("type").value;
            const imageName = `${selectedData.image}.jpg`;
            const imageUrl = `https://epic.gsfc.nasa.gov/archive/${type}/${year}/${month}/${day}/jpg/${imageName}`;
            
            document.getElementById("earth-image").src = imageUrl;
        }
        imageMenu.addEventListener("click", displayImage);

        document.getElementById("submitButton").addEventListener("click", function(event) {
            event.preventDefault();

            const type = document.getElementById("type").value;
            const date = document.getElementById("date").value;
            let URL = createSearchURL(EpicApplication.currentImageType, date);
            
            if (EpicApplication.imageCaches[type].has(date)) {
                EpicApplication.imageData = EpicApplication.imageCaches[type].get(date);
                updateImageMenu(EpicApplication.imageData);
            } else {
                fetch(URL)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(`Something went wrong`);
                        }
                        return response.json();
                    })
                    .then(obj => {
                        EpicApplication.imageData = obj;
                        EpicApplication.imageCaches[type].set(date, obj);
                        updateImageMenu(EpicApplication.imageData);
                    })
                    .catch(err => {
                        console.error("Error:", err);
                    });
            }
        });

        /**
         * Fetches and updates the input field with the most recent available dates for images.
         */
        function recentAvailableDates() {

            let datesURL = `https://epic.gsfc.nasa.gov/api/${EpicApplication.currentImageType}/all`;

            if (EpicApplication.dateCache[EpicApplication.currentImageType]) {
                document.getElementById("date").max = EpicApplication.dateCache[EpicApplication.currentImageType];
                if (EpicApplication.imageCaches[currentType].has(dateInput.max)) {
                    EpicApplication.imageData = EpicApplication.imageCaches[currentType].get(dateInput.max);
                    updateImageMenu(EpicApplication.imageData);
                }
            } else { 
                fetch(datesURL)
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(`HTTP error! status: ${response.status}`);
                        }
                        return response.json();
                    })
                    .then(obj => {
                        EpicApplication.dateCache[EpicApplication.currentImageType] = obj[0].date;
                        document.getElementById("date").max = EpicApplication.dateCache[EpicApplication.currentImageType];
                        if (EpicApplication.imageCaches[EpicApplication.currentImageType].has(obj[0].date)) {
                            EpicApplication.imageData = EpicApplication.imageCaches[EpicApplication.currentImageType].get(obj[0].date);
                            updateImageMenu(EpicApplication.imageData);
                        }
                    })
                    .catch(err => {
                        console.error("Error fetching dates:", err);
                    });
                }
        }

        document.getElementById("type").addEventListener("change", function(event) {
            EpicApplication.currentImageType = event.target.value;
            recentAvailableDates();
        });
    });
})();